package com.example.demo.visual;

import com.example.demo.dominio.ClienteRepository;
//import com.example.demo.dominio.Estudiante;
import com.example.demo.dominio.Cliente;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.SingleSelectionModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

/**
 * Created by Guicho on 15/07/2017.
 */
@SpringUI
public class App extends UI {
    @Autowired
    ClienteRepository clienteRepository;

    @Override
    protected void init (VaadinRequest vaadinRequest){

        final VerticalLayout layout = new VerticalLayout();
        MenuBar barmenu = new MenuBar();
        layout.addComponent(barmenu);


        final Label selection = new Label("");
        layout.addComponent(selection);

        MenuBar.Command mycommand = new MenuBar.Command() {
           public void menuSelected(MenuBar.MenuItem selectedItem) {
                selection.setValue("Insertar " +
                        selectedItem.getText() +
                     " ");
           }
        };
        barmenu.addItem("Clientes", null, mycommand);
        barmenu.addItem("Proveedores", null, mycommand);
        barmenu.addItem("Producto", null, mycommand);


        HorizontalLayout hlayout = new HorizontalLayout();

        final TextField nit = new TextField("Nit");
        final TextField nombre = new TextField("Nombre");
        final TextField apellido = new TextField("Apellido");
        final TextField edad = new TextField("Edad");

        Grid<Cliente> grid = new Grid<>();
        grid.addColumn(Cliente::getId).setCaption("ID");
        grid.addColumn(Cliente::getNit).setCaption("Nit");
        grid.addColumn(Cliente::getNombre).setCaption("Nombre");
        grid.addColumn(Cliente::getApellido).setCaption("Apellido");
        grid.addColumn(Cliente::getEdad).setCaption("Edad");
        grid.setWidth("800px");

        Button add = new Button("Insertar");
        add.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Cliente e = new Cliente();
                e.setNit(nit.getValue());
                e.setNombre(nombre.getValue());
                e.setApellido(apellido.getValue());
                e.setEdad(edad.getValue());

                clienteRepository.save(e);
                grid.setItems(clienteRepository.findAll());

                nit.clear();
                nombre.clear();
                apellido.clear();
                edad.clear();
                Notification.show("Cliente Agregado Correctamente");

            }
        });


        Button eliminar = new Button("Eliminar");
        add.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {

                grid.setSelectionMode(Grid.SelectionMode.MULTI);

                grid.addSelectionListener(event -> {
                    Set<Cliente> selected = event.getAllSelectedItems();
                    Notification.show(selected.size() + " Registro Seleccionado");
                });
                //Notification.show("Registro Eliminado");
            }

        });


        hlayout.addComponents(nit, nombre, apellido, edad);
        layout.addComponents(hlayout, grid, add, eliminar);
        layout.setComponentAlignment(add, Alignment.BOTTOM_LEFT);
        layout.setComponentAlignment(eliminar, Alignment.BOTTOM_LEFT);
        setContent(layout);
    }
}
