package com.example.demo.dominio;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Fernando Ambrosio on 19/07/2017.
 */
public interface ClienteRepository extends JpaRepository<Cliente,Long> {
}
