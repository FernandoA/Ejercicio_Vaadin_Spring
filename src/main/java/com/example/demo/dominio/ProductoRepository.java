package com.example.demo.dominio;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Fernando Ambrosio on 20/07/2017.
 */
public interface ProductoRepository extends JpaRepository<Producto,Long> {
}
