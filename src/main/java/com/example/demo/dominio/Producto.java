package com.example.demo.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Fernando Ambrosio on 05/07/2017.
 */

@Entity
public class Producto {

    private int cod_Producto;
    private String nom_Producto;
    private String cliente;
    private float costo;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Producto() {
    }

    public Producto(int cod_Producto, String nom_Producto, String cliente, float costo) {
        this.cod_Producto = cod_Producto;
        this.nom_Producto = nom_Producto;
        this.cliente = cliente;
        this.costo = costo;
    }

    public int getCod_Producto() {
        return cod_Producto;
    }

    public void setCod_Producto(int cod_Producto) {
        this.cod_Producto = cod_Producto;
    }

    public String getNom_Producto() {
        return nom_Producto;
    }

    public void setNom_Producto(String nom_Producto) {
        this.nom_Producto = nom_Producto;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }
}
